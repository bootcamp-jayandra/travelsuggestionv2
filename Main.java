import asset.Appearance;
import user.Customer;
import tour.Destination;
import util.*;

public class Main extends Appearance{
	private static InputOption input = new InputOption();
	private static Customer customer = new Customer();
	private static int flowCounter = 0,
					   destinationChoice = -1;
	
	private static final Destination[] destinations = {
		new Destination("Lombok", "Snorkling", 2170000L, 125000L, 75000L, 250000L),
		new Destination("Bangkok", "Shopping", 3780000L, 155000L, 55000L, 300000L),
		new Destination("Singapore", "Universal Studio", 1200000L, 170000L, 85000L, 360000L),
		new Destination("Tokyo", "Ski", 4760000L, 170000L, 105000L, 325000L)
	};
	
	public static void main(String[] args){
		while (flowCounter < 5){	
			switch (flowCounter){
				case 0:
					appInitialization();
					break;
				case 1:
					showDestinationAvailable(customer);
					destinationChoice = input.inputNumberChoice(customer);
					if (destinationChoice == -1 || destinationChoice == 99){
						flowCounter += destinationChoice;
						break;
					}
					flowCounter++;
					break;
				case 2:
					showDestinationDetail(customer.getDestination(destinationChoice));
					flowCounter += input.inputContinueFlow();
					break;
				case 3:
					showFinalPayment(customer, customer.getDestination(destinationChoice));
					flowCounter += input.inputContinueFlow();
					break;
				case 4:
					flowCounter = 0;
					break;
			}
		}
		showGoodbyeMessage();
	}
	
	private static void appInitialization(){
		customer = new Customer();
		showInitialOption();
		showInputDescription(input.inputProgramContinue());			
		customer.inputCustomerDetails();
		addDestinationToCustomer(destinations, customer);
		flowCounter++;
	}
	
	private static void addDestinationToCustomer(Destination[] destinations, Customer customer){
		for (Destination destination : destinations){
			if ((customer.getBudget() - destination.countTotalPrice(customer)) >= 0){
				customer.addDestination(destination);
			}
		}
	}
}