package util;

import java.util.Scanner;

public class InputValidator{
	protected static Scanner scan = new Scanner(System.in);

	protected int safeInteger(){
		while (true){
			try {
				return Integer.valueOf(scan.nextLine());
			}
			catch (Exception e){
				System.out.printf("\nEnter the correct integer: ");
			}
		}
	}
	
	protected String safeAlphabetString(){
		String alphabetString;
		do {
			alphabetString = scan.nextLine();
			if (alphabetString.equals("") || alphabetString.matches(".?[^A-z ]+.?")){
				System.out.printf("\nEnter the correct alphabet string: ");
			}
		}
		while (alphabetString.equals("") || alphabetString.matches(".?[^A-z ]+.?"));
		return alphabetString;
	}
	
	protected long moneyStringToLong(){
		String money = scan.nextLine();
		String moneyResult = "";
		String[] moneySeparated = money.split(",");
		
		int counter = moneySeparated.length-1;
		while (counter>=0){
			if (moneySeparated[counter].length() > 3 || moneySeparated[counter].matches(".?[^0-9]+.?")){
				System.out.printf("\nRe-input the money using [,] as thousand separator: Rp");
				money = scan.nextLine();
				moneySeparated = money.split(",");
				counter = moneySeparated.length-1;
			}
			else{
				moneyResult = moneySeparated[counter] + moneyResult;
				counter--;
			}
		}
		return Long.valueOf(moneyResult);
	}
}