package util;

import java.util.Scanner;
import user.Customer;

public class InputOption extends InputValidator{
	private static Scanner scan = new Scanner(System.in);
	
	public boolean inputProgramContinue(){
		String continueProgram = "";
		do {
			System.out.printf("\nYour answer: ");
			continueProgram = scan.nextLine();
			if (!(continueProgram.equalsIgnoreCase("Y") || continueProgram.equalsIgnoreCase("N"))){
				System.out.printf("Please input the proper answer!\n");
			}
			else {
				if (continueProgram.equalsIgnoreCase("N")){
					return false;
				}
			}
		}
		while (!(continueProgram.equalsIgnoreCase("Y") || continueProgram.equalsIgnoreCase("N")));
		return true;
	}
	
	public int inputNumberChoice(Customer customer){
		System.out.printf("\nYour number of choice: ");
		int destination;
		do {
			destination = super.safeInteger();
			if (destination == 0 || destination == 99){
				return inputBackOrStop(destination);
			}
			else if (destination > 0 && destination <= customer.getAllowedDestination().size()){
				return destination-1;
			}
			else {
				System.out.printf("\nEnter the proper destination choice: ");
			}
		}
		while (destination != 99 || destination != 0 || !(destination > 0 && destination < 5));
		return 0;
	}
	
	public int inputContinueFlow(){
		int choice;
		do {
			System.out.printf("\nYour number of choice: ");
			choice = super.safeInteger();
			if (choice != 99 && choice != 0 && choice != 1){
				System.out.printf("Input the proper number of choice: ");
			}
		}
		while (choice != 99 && choice != 0 && choice != 1);
		return inputBackOrStop(choice);
	}
	
	private int inputBackOrStop(int choice){
		switch (choice){
			case 0:
				return -1;
			case 99:
				return 99;
		}
		return choice;
	}
}