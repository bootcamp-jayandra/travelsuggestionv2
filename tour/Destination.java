package tour;

import user.Customer;

public class Destination{
	private String name, picnicName;
	private long roundTrip, hotelPrice, culinaryPrice, picnicPrice;
	private long totalPrice;
	
	public Destination(String name, String picnicName, 
						long roundTrip, long hotelPrice, long culinaryPrice, long picnicPrice){
		this.name = name;
		this.picnicName = picnicName;
		this.roundTrip = roundTrip;
		this.hotelPrice = hotelPrice;
		this.culinaryPrice = culinaryPrice;
		this.picnicPrice = picnicPrice;
	}
	
	public long countTotalPrice(Customer customer){
		int totalPeople = customer.getTotalPeople();
		int totalDay = customer.getTotalDay();
		this.totalPrice = 
			totalPeople * (this.roundTrip + this.picnicPrice + (totalDay * (this.culinaryPrice + this.hotelPrice)));
		return this.totalPrice;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getPicnicName(){
		return this.picnicName;
	}
	
	public long getRoundTrip(){
		return this.roundTrip;
	}
	
	public long getHotelPrice(){
		return this.hotelPrice;
	}
	
	public long getCulinaryPrice(){
		return this.culinaryPrice;
	}
	
	public long getPicnicPrice(){
		return this.picnicPrice;
	}
	
	public long getTotalPrice(){
		return this.totalPrice;
	}
}