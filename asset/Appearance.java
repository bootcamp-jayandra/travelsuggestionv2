package asset;

import user.Customer;
import tour.Destination;

import java.util.List;
import java.util.ArrayList;

public class Appearance{
						  
	protected static void showInitialOption(){
		showAppDescription();
		System.out.printf("| Do you want to continue? (Y/N)                |\n");
		showBottomBorder();
	}
	
	protected static void showInputDescription(boolean isProgramContinue){
		if (isProgramContinue){
			showAppDescription();
			System.out.printf("| Please input your full name, total person,    |\n");
			System.out.printf("| and total day for the travel recommendation   |\n");
			showBottomBorder();	
		}
		else {
			showGoodbyeMessage();
			System.exit(0);
		}
	}
	
	protected static void showDestinationAvailable(Customer customer){
		showAppDescription();
		System.out.printf("| Hi, %-41s |\n", customer.getName());
		System.out.printf("| Please choose destination below!              |\n");
		if (customer.getAllowedDestination().isEmpty()){
			showNoAllowedDestination();
		}
		else {
			int index = 1;
			for (Destination destination : customer.getAllowedDestination()){
				System.out.printf("| (%d) %-41s |\n", index, destination.getName());
				index++;
			}
			showAppGeneralMenu();
		}
	}
	
	protected static void showDestinationDetail(Destination destination){
		showAppDescription();
		System.out.printf("| >>> Base price for %-26s |\n", destination.getName());
		System.out.printf("|     Round trip/person        : Rp%-,12d |\n", destination.getRoundTrip());
		System.out.printf("|     Hotel accomodation/person: Rp%-,12d |\n", destination.getHotelPrice());
		System.out.printf("|     Culinary tour/person     : Rp%-,12d |\n", destination.getCulinaryPrice());
		System.out.printf("|     %-16s         : Rp%-,12d |\n", destination.getPicnicName(), 
															 destination.getPicnicPrice());
		System.out.printf("|                                               |\n");
		System.out.printf("| (1) Continue to payment                       |\n");
		showAppGeneralMenu();
	}
	
	protected static void showFinalPayment(Customer customer, Destination destination){
		long totalPeople = new Long(customer.getTotalPeople());
		long totalDay = new Long(customer.getTotalDay());
		showAppDescription();
		System.out.printf("| Hi, %-41s |\n", customer.getName());			
		System.out.printf("| Here is your payment details!                 |\n");
		System.out.printf("| Destination             : %-19s |\n", destination.getName());
		System.out.printf("| Round trip total        : Rp%-,17d |\n", 
							destination.getRoundTrip()*totalPeople);
		System.out.printf("| Hotel accomodation total: Rp%-,17d |\n",
							destination.getHotelPrice() * totalPeople * totalDay);
		System.out.printf("| Culinary tour total     : Rp%-,17d |\n",
							destination.getCulinaryPrice() * totalPeople * totalDay);
		System.out.printf("| %-24s: Rp%-,17d |\n", destination.getPicnicName(), 
							destination.getPicnicPrice() * totalPeople);
		System.out.printf("| >>> TOTAL               : Rp%-,17d |\n", destination.getTotalPrice());
		System.out.printf("|                                               |\n");
		System.out.printf("| (1) Restart App                               |\n");
		showAppGeneralMenu();
	}
	
	private static void showNoAllowedDestination(){
		System.out.printf("| Unfortunately, we can't find any place with   |\n");
		System.out.printf("| your budget right now                         |\n");
		showAppGeneralMenu();
	}
	
	private static void showAppGeneralMenu(){
		System.out.printf("|                                               |\n");
		System.out.printf("| (0) Back                                      |\n");
		System.out.printf("| (99) Close the app                            |\n");
		showBottomBorder();
	}
	
	private static void showAppDescription(){
		clearTerminalDisplay();
		System.out.printf("=================================================\n");
		System.out.printf("|                                               |\n");
		System.out.printf("|     Welcome to the Travel Suggestion App!     |\n");
		System.out.printf("|                                               |\n");
		System.out.printf("| We'll give the best recommendation for you,   |\n");
		System.out.printf("| based on your budget!                         |\n");
		System.out.printf("|                                               |\n");
	}
	
	protected static void showGoodbyeMessage(){
		showAppDescription();
		System.out.printf("|          The application is stopped!          |\n");
		System.out.printf("|                                               |\n");
		System.out.printf("|     Thank you for using our application!      |\n");
		showBottomBorder();	
	}
	
	private static void showBottomBorder(){
		System.out.printf("|                                               |\n");
		System.out.printf("=================================================\n");
	}
		
	private static void clearTerminalDisplay(){
		try {
			if (System.getProperty("os.name").contains("Windows")){
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			}
			else {
				Runtime.getRuntime().exec("clear");
			}
		}
		catch (Exception exception){
			System.out.printf("Sorry, you're currently using unknown OS!");
		}
	}
}