package user;

import java.util.List;
import java.util.ArrayList;
import util.InputValidator;
import tour.Destination;

public class Customer extends InputValidator{
	
	private String name;
	private int totalPeople;
	private int totalDay;
	private long budget;
	private List <Destination> allowedDestination = new ArrayList<Destination>();
	
	public void inputCustomerDetails(){
		inputName();
		inputTotalPeople();
		inputTotalDay();
		inputBudget();
		System.out.printf("\n        ;;PRESS [ENTER] TO CONTINUE;;");
		scan.nextLine();
	} 
	
	private void inputName(){
		System.out.printf("\nYour full name: ");
		this.name = super.safeAlphabetString();
	}
	
	private void inputTotalPeople(){
		System.out.printf("\nTotal people who'll join: ");
		this.totalPeople = super.safeInteger();
	}
	
	private void inputTotalDay(){
		System.out.printf("\nTotal day you'll be in: ");
		this.totalDay = super.safeInteger();
	}
	
	private void inputBudget(){
		System.out.printf("\nYour budget (Using [,] as thousand separator): Rp");
		this.budget = super.moneyStringToLong();
	}
	
	public void addDestination(Destination destination){
		this.allowedDestination.add(destination);
	}
	
	public String getName(){
		return this.name;
	}
	
	public long getBudget(){
		return this.budget;
	}
	
	public int getTotalPeople(){
		return this.totalPeople;
	}
	
	public int getTotalDay(){
		return this.totalDay;
	}
	
	public List<Destination> getAllowedDestination(){
		return this.allowedDestination;
	}
	
	public Destination getDestination(int index){
		return this.allowedDestination.get(index);
	}
}